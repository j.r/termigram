#!/usr/bin/env python

from pyrogram import Client, filters
import re
import subprocess
import html
import os
import pathlib

app = Client("tg", api_id=117459, api_hash="da4bef98585349c1e29313f1a0abac72")

homepath = str(pathlib.Path().absolute())

@app.on_message(filters.outgoing & filters.command("cli", prefixes="."))
async def cli(client, message):
    try:
        try:
            process = subprocess.run(
                message.text.replace(".cli", ""),
                capture_output=True,
                shell=True,
                timeout=10,
            )
        except subprocess.TimeoutExpired:
            await message.edit(
                "$ " + message.text.replace(".cli", "") + "\n```Command Timed Out```"
            )
        path = False
        if message.text.split(" ")[1] == "cd":
            path = message.text.replace(".cli cd", "").strip()
            path = path if path!="" else homepath


        ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
        output = ansi_escape.sub('', process.stdout.decode("utf-8"))
        stderr = process.stderr.decode("utf-8")
        await message.edit_text(
            str(pathlib.Path().absolute()).replace(homepath, "~")+"$ "
            + html.escape(message.text.replace(".cli", ""))
            + "\n<pre>"
            + html.escape(output)
            + html.escape(stderr)
            + "</pre>",
            parse_mode="html"
        )
    except Exception as e:
        await message.edit_text(
            str(pathlib.Path().absolute()).replace(homepath, "~")+"$ "
            + html.escape(message.text.replace(".cli", ""))
            + "\n<pre>"
            + html.escape(str(e))
            + "</pre>",
            parse_mode="html"
        )
    try:
        if path:
            os.chdir(path)
    except Exception as e:
        await message.edit_text(
            str(pathlib.Path().absolute()).replace(homepath, "~")+"$ "
            + html.escape(message.text.replace(".cli", ""))
            + "\n<pre>"
            + html.escape(str(e))
            + "</pre>",
            parse_mode="html"
        )


@app.on_message(filters.outgoing & filters.command("clip", prefixes="."))
async def clip(client, message):
    process = subprocess.Popen(
        message.text.replace(".clip", ""),
        shell=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
    )
    out, _ = process.communicate(input=message.reply_to_message.text.encode())
    await message.edit(
        "$ " + message.text.replace(".clip", "") + "\n```" + out.decode("utf-8") + "```"
    )


app.run()
